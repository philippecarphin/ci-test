#include <string.h>
#include <stdio.h>

extern char **environ;

int main(void){

    char tmpdir_name[] = "TMPDIR=";
    size_t len = strlen(tmpdir_name);
    for(char **var_p = environ; *var_p != NULL; var_p++){
        char *var = *var_p;
        if(strncmp(var, tmpdir_name, len) == 0){
            fprintf(stderr, "TMPDIR entry at %p: '%s'\n", var, var);
        }
    }
}
