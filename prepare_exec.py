#!/usr/bin/env -S python3 -u

import os
import sys

tmpdir1 = os.environ['TMPDIR']
print(f"os.environ['TMPDIR'] = {tmpdir1}", file=sys.stderr)

tmpdir2 = subprocess.run("echo $TMPDIR", shell=True, universal_newlines=True).stdout
print(f"$(echo $TMPDIR) = {tmpdir2}", file=sys.stderr)

print(f"Running this program:", file=sys.stderr)
with open("/Users/pcarphin/custom-executor-demo/print_environ.c") as f:
    print(f.read(), file=sys.stderr)
subprocess.run("/Users/pcarphin/custom-executor-demo/print_environ")


